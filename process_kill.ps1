if(-not ((Get-Process -Name "TeamViewerLogMonitor" ) -eq $null))
{
    Write-Host "Leállítás TeamViewerLogMonitor" -BackgroundColor Red
    Get-Process -Name "TeamViewerLogMonitor" | Stop-Process
}
else
{
    Write-Host "Nem fut TeamViewerLogMonitor!" -BackgroundColor Green
}
if(-not ((Get-Process -Name "ffmpeg") -eq $null))
{
    Write-Host "Leállítás ffmpeg" -BackgroundColor Red
    Get-Process -Name "ffmpeg" | Stop-Process
}
else
{
    Write-Host "Nem fut ffmpeg!" -BackgroundColor Green
}
pause