﻿#### RESET VARIABLES ##############
$tmProgramFilesLogPath = ""
$tmLogMonitor = ""
$ffmpegPath = ""
$recordPath = ""
###################################

####### Módosítható változók #############################################################
$tmProgramFilesLogPath = 'C:\Program Files (x86)\TeamViewer\TeamViewer14_Logfile.log'
$tmLogMonitor = "$PSScriptRoot\DebugLogs\TeamViewerLogMonitor_Debug.log"
$ffmpegPath = "$PSScriptRoot\ffmpeg\bin\ffmpeg.exe"
$recordPathStart = $PSScriptRoot+"\" #Mappa végén \
##########################################################################################

$fileName = (Get-WmiObject -Class Win32_BIOS | Select-Object SerialNumber).SerialNumber
$logFileName = $fileName + ".log"
$logPath = $recordPathStart + $logFileName

function Write-Log {
    param(
    [parameter(Mandatory=$true)]
    [string]$Text,
    [parameter(Mandatory=$true)]
    [ValidateSet("WARNING","ERROR","INFO")]
    [string]$Type
    )

    [string]$logMessage = [System.String]::Format("[$(Get-Date)] -"),$Type, $Text
    Add-Content -Path $logPath -Value $logMessage
}
## (ha többször indul el a logmonitor nem érzékeli a csatlakozást)

$tproc = Get-Process -Name "TeamViewerLogMonitor" -ErrorAction SilentlyContinue | Stop-Process 
Write-Host "Tisztítás...." -ForegroundColor Green 
Write-Host "FutóTeamViewerLog (hasExited):" $tproc.HasExited
Start-sleep 1
Write-Host "Log maradvány törlése...." -ForegroundColor Green

if(Test-path -path $tmLogMonitor)
{
    Write-Host "Törölve: " $tmLogMonitor 
    Remove-Item -path $tmLogMonitor 
}
else
{
    Write-Host "Nem kellett törölni: " $tmLogMonitor
}


Get-Process -Name "ffmpeg"  -ErrorAction SilentlyContinue | Stop-Process

# Start-Process -filepath .\TeamViewerLogMonitor.exe -WindowStyle hidden
start-sleep 1
Start-Process -filepath $PSScriptRoot\TeamViewerLogMonitor.exe -WindowStyle hidden
$TVProc = Get-Process -Name "TeamViewerLogMonitor" | Out-String


Write-Log -Type INFO -Text "Start Script"
Write-Log -Type INFO -Text $TVProc
Write-Host "Start..."

##### Rendszergazda vizsgálat #####
$currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
$logTextSend = "Rendszergazda jog: " + $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
Write-Log -Type INFO -Text $logTextSend
Write-Host $logTextSend
###################################
while($TVPoc.Responding -eq $False)
{
    Write-host "TeamViewer Kapcsolatfigyelő indulására várás..."
    Start-Sleep
}
Write-Host "TeamViewer Kapcsolatfigyelő indul!" -ForegroundColor "Green"
# Hozza létre a logot ezért várunk!
Start-Sleep 2 
Write-Host "TeamViewer Kapcsolatfigyelő elindult!" -ForegroundColor "Green"

While(1 -eq 1)
{
$log = Get-Content $tmLogMonitor -Tail 5
    If($log | where {$_.contains("New Connection detected")})
    {
        
        # $getdate = (get-date).year+(get-date).month+(get-date).day+(get-date).hour+(get-date).minute
        $getdate = Get-Date
        $getdate = $getdate.ToUniversalTime().ToString('yyyyMMdd-HHmmss')
        $recordpath=$recordPathStart+$getdate+".mkv"
        $recordpath
        $ffmpegPath
        $procid = start-process $ffmpegPath -ArgumentList "-f", "gdigrab", "-framerate 30","-i", "desktop ", $recordpath -WindowStyle Minimized -PassThru
        Write-host "Csatlakozás, Rögzítés Start!"
        $EndBoolean = $false
        $logTextSend = "ReadedLog from: " + $tmLogMonitor +  "Log: " + $log
        Write-Log -Type INFO -Text $logTextSend
        Write-Log -Type INFO -Text  $recordPath
        $logTextSend = "ffmpegPath: " + $ffmpegPath
        Write-Log -Type INFO -Text $logTextSend
        $logTextSend = "procid (ffmpeg.exe Process Details) (Responding): " + $procid.Responding
        Write-Log -Type INFO -Text $logTextSend
        Write-Log -Type INFO -Text " ======== Rögzítés elkezdve! ========" 
        
        while($EndBoolean -eq $False)
        {
        $logPR = Get-Content $tmProgramFilesLogPath -Tail 10
        If($logPR | where {$_.contains("DesktopThread ended")})
        {
           $procid.CloseMainWindow()
           #Ha változóból nem megy
           get-process ffmpeg | %{ $_.closemainwindow() } 
           # Stop-Process -id $procid
           start-sleep 1         
           $EndBoolean = $true
           Write-host "Rögzítés Befejezve!"
           $recordpath = ""
           $logTextSend = "ReadedLog from: " + $tmProgramFilesLogPath +  "Log: " + $logPR
           Write-Log -Type INFO -Text  $logTextSend
           $logTextSend = "procid (ffmpeg.exe Process Details) (HasExited): " + $procid.hasExited
           Write-Log -Type INFO -Text $logTextSend
           Write-Log -Type INFO -Text  " ======== Rögzítés befejezve! ========"
        }
        }

    }
        
}

